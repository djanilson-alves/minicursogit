The volume in which a character appears is indicated in parentheses after his or her name. Where there is no indication, the character appears in all three volumes. (Note: FR = The Fellowship of the Ring, TT = The Two Towers, and RK = Return of the King.)

Frodo Baggins A young well-to-do hobbit. When he discovers that the magic ring left to him by his eccentric "Uncle" Bilbo is the One Ring, he reluctantly takes on the quest to destroy it. Often referred to as the Ringbearer.

Gandalf the Grey A wizard best known among hobbits for his fireworks and mischievous sense of humor, but actually one of the greatest powers of Middle-earth. He reveals the truth about the Ring to Frodo and acts as a guide and counselor. The leader of the Fellowship. Other names: Stormcrow, Mithrandir, the Grey Pilgrim, the White Rider, Tharkûn, Olórin, Incánus, and Gandalf the White.

Samwise "Sam" Gamgee A hobbit, Frodo's gardener at home, and his servant and friend on the quest. Sam is very fond of stories about dragons and elves. A member of the Fellowship, he stays with Frodo after it is broken.

Meriadoc "Merry" Brandybuck A young hobbit, one of Frodo's many cousins. A member of the Fellowship. Friend of Treebeard the Ent and companion of King Théoden of Rohan.

Peregrin "Pippin" Took A young hobbit, one of Frodo's many cousins. A member of the Fellowship. Later, a member of the Tower Guard of Minas Tirith.

Aragorn The heir of Isildur and the rightful king of Gondor, a mighty warrior and healer. He was raised by the elves of Rivendell and now lives in exile as leader of the Dúnedain, the Rangers of the North. The time has now come for him to reclaim the throne. He leads the Fellowship after Gandalf's fall. Other names: Strider, Elessar, Elfstone, Dúnadan.

Legolas Son of Thranduil, king of the Mirkwood elves. He is a great archer with keen eyesight. A member of the Fellowship. Close friend of Gimli the dwarf and companion of Aragorn.

Gimli Son of Glóin, a dwarf of the Lonely Mountain. A member of the Fellowship. Close friend of Legolas the elf and companion of Aragorn. He falls in love with Galadriel.

Boromir (FR, TT) Son of Denethor, the Steward of Gondor. Sent to Rivendell to decipher a prophetic dream, he joins the Fellowship. He believes that the Ring should be used as a weapon against the Enemy and that trying to destroy it is foolish. He succumbs to the temptation of the Ring and tries to take it from Frodo. He dies defending Merry and Pippin from orcs.

Sauron A powerful spirit of evil who now takes the form of a disembodied flaming eye, Sauron represents the worst aspects of power — especially greed, cruelty, and the desire for domination. He created the One Ring long ago as a weapon, lost it nearly three thousand years ago, and now needs it to achieve his conquest of Middle-earth. Other names: the Enemy, the Dark Lord, the Necromancer, the Eye, the Lord of the Rings.

The Nazgûl, Servants of the Enemy Once great kings, but Sauron gave nine rings to these men and enslaved them under the One Ring. They are generals for his armies, but he also sends them to capture Frodo and the Ring. The Witchking leads them. Other names: Black Riders, Ringwraiths, the Nine.

Gollum An earlier Ringbearer. He carried the Ring, which gave him an unnaturally long life. He was living on an island in an underground lake when he lost the Ring to Bilbo, but he wants it back so badly that he has emerged to search for it. Although he fears Sauron and revealed the Ring's location to him, he also becomes Frodo's guide into Mordor. Despite the Ring's corrupt influence, Frodo still believes Gollum can be saved. Other names: Sméagol, Slinker, Stinker.

Bilbo Baggins (FR, RK) Frodo's much older cousin, considered the most eccentric hobbit in the Shire, who adopted Frodo after the younger hobbit's parents died. He won the Ring from Gollum in a riddle game 60 years before the start of the novel, and he passes it on to Frodo when he leaves the Shire for good. The only person to have voluntarily given up the Ring.

Tom Bombadil (FR) A nature spirit of the ancient world who helps Frodo in the Old Forest and on the Barrowdowns. The Ring has no effect on him. Other names: the Master, Eldest, Iarwain Ben-adar, Forn, Orald.

Glorfindel (FR) An elf-lord, sent by Elrond to help Frodo reach Rivendell during the first stage of the quest.

Elrond (FR, RK) Master of Rivendell, a place of refuge and healing. He hosts the Council that decides to destroy the Ring. His halfelven lineage allows his daughter Arwen to choose between the Undying Lands and mortality in Middle-earth.

Arwen Evenstar (FR, RK) Daughter of Elrond. She forsakes her elven immortality to betroth herself to Aragorn, although her father will not allow them to marry unless Aragorn reclaims the throne of Gondor. Other names: Undómiel.

Galadriel (FR, RK) Lady of the Golden Wood of Lothlórien, Galadriel rules one of the last elven strongholds in Middle-earth. Very old and very wise, she gives the Fellowship shelter and advice after Gandalf's fall. Her parting gifts to each member of the Fellowship become immensely significant.

Saruman the White The head of the order of wizards. Although he was once the wisest, lust for power has led him to side with Sauron. The magic of his voice can seduce or overwhelm the will of most men. Saruman is obsessed with machinery, cutting down forests to fuel his engines and forges. He hopes to take the Ring for himself and take Sauron's place as Lord of the Rings. Other names: Saruman the Wise, Saruman of Many Colors, the Voice, Sharkey.

Éomer (TT, RK) Son of Éomund. Nephew of Théoden (King of Rohan) and Éowyn's brother. Third Marshall of the Riddermark. Loyal captain of the horselords and a valiant warrior. He suspects Wormtongue's treachery.

Théoden (TT, RK) King of Rohan, a nation of horsemen and warriors. Wormtongue has played on his age and infirmity to keep the king and his warriors useless at home, but Gandalf counsels him back to life and valor.

Éowyn (TT, RK) Niece of Théoden (King of Rohan). She serves her uncle in his old age, but she desires to fight with a sword and die bravely. Many see her as an ice maiden — beautiful, but chilled by despair. She falls hopelessly in love with Aragorn, then rides secretly into battle and destroys the Witchking of the Nazgûl. Sister of Éomer.

Grìma Wormtongue (TT, RK) Counselor to Théoden, but actually an agent of Saruman, he keeps Théoden weak so that Saruman can move his armies through Rohan uncontested.

Shadowfax The greatest horse in Middle-earth, this silver-grey stallion chooses to accept only Gandalf as his rider.

Treebeard (TT, RK) The oldest mortal creature in Middle-earth. Treebeard is an Ent, one of the shepherds of trees, and looks much like a stumpy old oak tree himself except that he can walk and talk. He speaks for the natural world, which otherwise could not speak for itself. Other names: Fangorn.

Quickbeam (TT) An Ent who entertains Merry and Pippin during the Entmoot. He is one of the youngest Ents, and so moves and thinks closer to the speed of the shorter-lived hobbits; the other Ents describe him as "hasty." Other names: Bregalad.

Shelob (TT) An ancient evil, Shelob is a giant spider whose webs block the pass of Cirith Ungol. She embodies insatiable hunger and greed, but she has no interest in the Ring.

Faramir (TT, RK) Younger son of Denethor and brother of Boromir. Wiser and more learned than his brother, although also an admirable warrior. Denethor constantly compares his sons, and Faramir cannot seem meet his father's expectations. Nevertheless, Faramir resists the temptation of the Ring where his brother could not, and he helps Frodo on his journey.

Denethor (RK) Steward of Gondor, father of Boromir and Faramir. He rules the realm of Gondor and city of Minas Tirith in the generations-long absence of the True King. Once a wise leader, his own pride and the deceptions of the Enemy have led him to madness and despair.

Beregond (RK) A member of the elite Tower Guard of Minas Tirith. He is devoted to Faramir and befriends Pippin when the hobbit joins the guard.

Barliman Butterbur (FR, RK) The friendly yet frazzled and absent-minded innkeeper of the Prancing Pony in Bree.


